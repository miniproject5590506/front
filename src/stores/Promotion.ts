import type { Promotion } from '@/types/Promotion'
import { defineStore } from 'pinia'
import { computed, ref, watch } from 'vue'
import { useReceiptStore } from './receipt'
import POSViewVue from '@/views/POS/POSView.vue'
import type { Product } from '@/types/Product'
import { useproductStore } from './Product'
import { type ReceiptItem } from '@/types/ReceiptItem'

export const usePromotionStore = defineStore('Promotion', () => {
  const productStore = useproductStore()
  const receiptStore = useReceiptStore()
  const promotionConfirmDialog = ref(false)
  const promotionConditonCardDialog = ref(false)

  const promotionDialog = ref(false)

  const getStatus = (startDate: string, endDate: string): 'enable' | 'disable' => {
    const currentDate = new Date()

    const year = currentDate.getFullYear()
    const month = (currentDate.getMonth() + 1).toString().padStart(2, '0')
    const day = currentDate.getDate().toString().padStart(2, '0')

    const formattedDate = `${year}-${month}-${day}`

    const start = new Date(startDate.replace(/-/g, '/')) // Fix for Safari and Firefox
    const end = new Date(endDate.replace(/-/g, '/'))

    // Use getTime() for proper comparison
    return start.getTime() <= currentDate.getTime() && currentDate.getTime() <= end.getTime()
      ? 'enable'
      : 'disable'
  }

  const promotions = ref<Promotion[]>([
    {
      id: 1,
      code: 'WELCOME10',
      name: 'New Customer Welcome',
      startdate: '2024-01-01',
      enddate: '2024-02-28',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 201,
          productid: 1,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: null,
          conditionPrice: 100,
          discountPercent: 10,
          discountPrice: null,
          status: null
        }
      ]
    },
    {
      id: 2,
      code: 'HAPPYHOUR',
      name: 'Happy Hour Special',
      startdate: '2024-01-01',
      enddate: '2024-02-31',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 202,
          productid: 2,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: null,
          conditionPrice: 100,
          discountPercent: null,
          discountPrice: 50,
          status: null
        }
      ]
    },
    {
      id: 3,
      code: 'LOYALTY5',
      name: 'Kimpromotion',
      startdate: '2022-12-01',
      enddate: '2024-01-01',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 203,
          productid: 1,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: null,
          conditionPrice: 0,
          discountPercent: 50,
          discountPrice: null,
          status: null
        }
      ]
    },
    {
      id: 4,
      code: 'Null code',
      name: 'Null promotion',
      startdate: '2024-01-01',
      enddate: '2024-11-30',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 204,
          productid: null,
          product: null,
          conditionQty: null,
          conditionPrice: 0,
          discountPercent: null,
          discountPrice: 500,
          status: null
        }
      ]
    },
    {
      id: 5,
      code: 'FREESAMPLE',
      name: 'Free Coffee Sample',
      startdate: '2024-01-01',
      enddate: '2024-01-30',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 202,
          productid: 6,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: null,
          conditionPrice: 100,
          discountPercent: 50,
          discountPrice: null,
          status: null
        }
      ]
    },
    {
      id: 6,
      code: 'SUMMER20',
      name: 'Summer Sale',
      startdate: '2024-01-01',
      enddate: '2024-08-31',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 206,
          productid: 4,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: null,
          conditionPrice: 70,
          discountPercent: null,
          discountPrice: 20,
          status: null
        }
      ]
    },
    {
      id: 7,
      code: 'Black1',
      name: 'Black friday',
      startdate: '2024-01-01',
      enddate: '2024-08-31',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 206,
          productid: 4,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: 3,
          conditionPrice: null,
          discountPercent: null,
          discountPrice: 60,
          status: null
        }
      ]
    },
    {
      id: 8,
      code: 'Black2',
      name: 'Black friday2',
      startdate: '2024-01-01',
      enddate: '2024-08-31',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 206,
          productid: 5,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: 3,
          conditionPrice: null,
          discountPercent: 50,
          discountPrice: null,
          status: null
        }
      ]
    },
    {
      id: 9,
      code: 'ช้างอยู่ไหน',
      name: 'วิกฤตต้มยำกุ้ง',
      startdate: '2024-01-01',
      enddate: '2024-08-31',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: [
        {
          id: 206,
          productid: 25,
          get product() {
            return productStore.searchProduct(this.productid!) || null
          },
          conditionQty: 1,
          conditionPrice: null,
          discountPercent: null,
          discountPrice: 20,
          status: null
        }
      ]
    }
  ])

  const clearCurrentPromotion = () => {
    currentPromotion.value = {
      id: -1,
      code: '',
      name: '',
      startdate: '2024-01-09',
      enddate: '2024-01-30',
      get status() {
        return getStatus(this.startdate, this.enddate)
      },
      PromotionItem: []
    }
  }

  // this is about search things

  const searchQuery = ref('')
  // chat gpt mod this to sort kub
  // use this to search in the seacrhbar
  const searchfilteredPromotions = computed(() => {
    const filtered = promotions.value.filter((promotion: Promotion) =>
      promotion.name.toLowerCase().includes(searchQuery.value.toLowerCase())
    )

    // Custom sorting function
    filtered.sort((a, b) => {
      // 'enable' promotions come first
      if (a.status === 'enable' && b.status !== 'enable') {
        return -1
      } else if (a.status !== 'enable' && b.status === 'enable') {
        return 1
      }

      // Sort by other criteria (you can adjust this based on your needs)
      // For example, sorting by start date
      const dateA = new Date(a.startdate).getTime()
      const dateB = new Date(b.startdate).getTime()

      return dateA - dateB
    })

    return filtered
  })

  //select promotion
  const selectedPromotionId = ref<number>()
  const selectedPromotion = computed(() => {
    if (selectedPromotionId.value !== null) {
      // Use the index to find the promotion in the promotions array
      const editedIndex = promotions.value.findIndex(
        (item) => item.id === selectedPromotionId.value
      )
      return promotions.value[editedIndex] || null
    }
    return null
  })

  const currentPromotion = ref<Promotion | null>({
    id: -1,
    code: '',
    name: '',
    startdate: '2024-01-09',
    enddate: '2024-01-30',
    get status() {
      return getStatus(this.startdate, this.enddate)
    },
    PromotionItem: []
  })

  // use this function to click and use promotion
  const usePromotion = () => {
    currentPromotion.value = selectedPromotion.value
    promotionDialog.value = false
    promotionConfirmDialog.value = false

    let i = 1
    if (
      currentPromotion.value?.PromotionItem !== null &&
      currentPromotion.value?.PromotionItem !== undefined
    ) {
      const promotionItems = currentPromotion.value.PromotionItem

      if (promotionItems !== undefined && promotionItems !== null && promotionItems.length > 0) {
        for (const promotionItem of promotionItems) {
          const {
            id: promotionId,
            productid: promotionProductid,
            discountPercent: discountPercent,
            conditionQty,
            conditionPrice,
            discountPrice: discountPrice,
            status
          } = promotionItem
          // const productName = productid ? productStore.searchProduct(productid)?.name : null;

          for (const receiptItem of receiptStore.receiptItems!) {
            const {
              id: receiptItemId,
              name: receiptName,
              price: receiptPrice,
              unit: receiptUnit,
              productId: receiptProductId,
              product: ReceiptProduct
            } = receiptItem
            console.log('Pass recieptItem ' + receiptItemId)
            console.log('receipt product id' + receiptProductId)

            console.log('loop round:' + i)
            i = i++

            // no product id
            if (promotionProductid === null) {
              console.log('no Id pass')
              // no id condition Price
              if (conditionPrice !== null && conditionQty === null) {
                console.log('conditionPrice pass!')
                //no id conditionPrice Discountpercent
                console.log('this is' + discountPercent)
                if (discountPercent !== null && discountPrice === null) {
                  console.log('discountpercent pass!')
                  // check
                  if (receiptStore.receipt.totalBefore >= conditionPrice) {
                    console.log('receiptPrice >= conditionPrice pass!')
                    // set Discount

                    receiptStore.receipt.promotionDiscount =
                      receiptStore.receipt.totalBefore * (discountPercent / 100)
                    console.log('setpass')
                  }
                }
                //no id conditionPrice Discountprice
                else if (discountPercent === null && discountPrice !== null) {
                  // check
                  if (receiptStore.receipt.totalBefore >= conditionPrice) {
                    console.log('this discount price =' + discountPrice)
                    // set Discount
                    receiptStore.receipt.promotionDiscount = discountPrice
                  }
                }
                // no id condition qty
              } else if (conditionPrice === null && conditionQty !== null) {
                //no id conditionQty Discountpercent
                if (discountPercent !== null && discountPrice === null) {
                  // check
                  if (receiptUnit >= conditionQty) {
                    // set Discount
                    receiptStore.receipt.promotionDiscount =
                      receiptStore.receipt.totalBefore * (discountPercent / 100)
                  }
                }
                //no id conditionQty Discountprice
                else if (discountPercent === null && discountPrice !== null) {
                  // check
                  if (receiptUnit >= conditionQty) {
                    // set Discount
                    console.log('discountPrice' + discountPrice)
                    receiptStore.receipt.promotionDiscount = discountPrice
                  }
                }
              }
            } else if (promotionProductid !== null) {
              console.log('Check promotionid Passed id:' + promotionProductid)
              console.log('this product id =', promotionProductid)
              console.log('this receiptItemId =', receiptItemId)
              console.log('this receiptItemId =', receiptItemId)
              if (productStore.searchProduct(promotionProductid)?.name === receiptName) {
                console.log(
                  ' kim hum yai ' + productStore.searchProduct(promotionProductid)?.name ===
                    receiptName
                )
                // have id condition Price
                if (conditionPrice !== null && conditionQty === null) {
                  //have id conditionPrice Discountpercent
                  console.log('have id condition Price Passed')
                  if (discountPercent !== null && discountPrice === null) {
                    console.log('reciept price =', receiptPrice)
                    console.log('conditionPrice =', conditionPrice)
                    if (receiptPrice * receiptUnit >= conditionPrice) {
                      receiptStore.receipt.promotionDiscount =
                        receiptStore.receipt.totalBefore * (discountPercent / 100)
                    } else {
                      console.log('receiptPrice>=conditionPrice Not passed')
                      return
                    }
                  }
                  //have id conditionPrice Discountprice
                  else if (discountPercent === null && discountPrice !== null) {
                    console.log('reciept price =', receiptPrice)
                    console.log('conditionPrice =', conditionPrice)
                    if (receiptPrice * receiptUnit >= conditionPrice) {
                      receiptStore.receipt.promotionDiscount = discountPrice
                    } else {
                      console.log('receiptPrice * receiptUnit >= conditionPrice Not passed')
                      return
                    }
                  }

                  // have id condition qty
                } else if (conditionPrice === null && conditionQty !== null) {
                  if (receiptUnit >= conditionQty) {
                    console.log('receiptUnit>=conditionQty')
                    //have id conditionQty Discountpercent
                    if (discountPercent !== null && discountPrice === null) {
                      receiptStore.receipt.promotionDiscount =
                        receiptStore.receipt.totalBefore * (discountPercent / 100)
                    }
                    //have id conditionQty Discountprice
                    else if (discountPercent === null && discountPrice !== null) {
                      receiptStore.receipt.promotionDiscount = discountPrice
                    }
                  }
                }
              }
            }
          }
        }
      } else {
      }
    }
  }

  return {
    promotionDialog,
    promotions,
    promotionConfirmDialog,
    getStatus,
    promotionConditonCardDialog,
    searchfilteredPromotions,
    searchQuery,
    currentPromotion,
    selectedPromotionId,
    selectedPromotion,
    usePromotion,
    clearCurrentPromotion
  }
})
