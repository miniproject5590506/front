import type { Employee } from '@/types/Employee'
import { defineStore } from 'pinia'
import { nextTick, ref } from 'vue'
import { onMounted } from 'vue'

export const useEmployStore = defineStore('employee', () => {
  const Employees = ref<Employee[]>([])
  //กำหนดหัวData table
  const headers = [
    {
      title: 'Employee ID',
      key: 'id',
      value: 'id'
    },
    {
      title: 'Employee Name',
      key: 'name',
      value: 'name'
    },
    {
      title: 'Tel',
      key: 'tel',
      value: 'tel'
    },
    {
      title: 'Status',
      key: 'status',
      value: (item: any | Employee) => item.status.join(', ')
    },
    {
      title: 'Check In',
      key: 'checkIn',
      value: 'checkIn'
    },
    {
      title: 'Check Out',
      key: 'checkOut',
      value: 'checkOut'
    },
    {
      title: 'Slary',
      key: 'salary',
      value: 'salary'
    },
    { title: '', key: 'actions', sortable: false }
  ]
  const form = ref(false)

  const dialog = ref(false)
  const dialogDelete = ref(false)
  const loading = ref(false)
  const initilEmployee: Employee = {
    id: -1,
    name: '',
    tel: '',
    status: ['PartTime'],
    salary: 0,
    dateIn: '',
    checkIn: '',
    checkOut: ''
  }
  //Employeeที่กำลังแก้ไข
  const editedEmployee = ref<Employee>(JSON.parse(JSON.stringify(initilEmployee)))
  // ข้อมูล

  let editedIndex = -1
  let lastId = 5
  function onSubmit() {}

  function closeDelete() {
    dialogDelete.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initilEmployee)
    })
  }
  function deleteItemConfirm() {
    Employees.value.splice(editedIndex, 1)
    closeDelete()
  }
  function editItem(item: Employee) {
    editedIndex = Employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialog.value = true
  }
  function deleteItem(item: Employee) {
    editedIndex = Employees.value.indexOf(item)
    editedEmployee.value = Object.assign({}, item)
    dialogDelete.value = true
    editedIndex = -1
    const index = Employees.value.findIndex((Employee) => Employee.id === item.id)
    if (index !== -1) {
      Employees.value.splice(index, 1)
    }
    closeDelete()
  }
  function closeDialog() {
    dialog.value = false
    nextTick(() => {
      editedEmployee.value = Object.assign({}, initilEmployee)
      editedIndex = -1
    })
  }
  async function save() {
    if (editedIndex > -1) {
      Object.assign(Employees.value[editedIndex], editedEmployee.value)
    } else {
      editedEmployee.value.id = lastId++
      Employees.value.push(editedEmployee.value)
    }
    closeDialog()
  }
  function initialize() {
    Employees.value = [
      {
        id: 1,
        name: 'Mod Dang',
        tel: '0811651848',
        status: ['PartTime', 'FullTime'],
        salary: 1000,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 2,
        name: 'Ja Ja',
        tel: '0814132218',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 3,
        name: 'Mind Grian',
        tel: '0815184623',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 4,
        name: 'Poy Fon',
        tel: '0814745158',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 5,
        name: 'Aoffy',
        tel: '0813568425',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 6,
        name: 'Pasi',
        tel: '0999999999',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 7,
        name: 'BlueBu',
        tel: '0888888888',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 8,
        name: 'Kimmy',
        tel: '0832156742',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      },
      {
        id: 9,
        name: 'Wittaya',
        tel: '0832568307',
        status: ['PartTime'],
        salary: 400,
        dateIn: '2022-01-08',
        checkIn: '08.00.55',
        checkOut: '16.00.48'
      }
    ]
  }

  onMounted(() => {
    initialize()
  })

  function searchfor(Input: string) {
    Employees.value = Employees.value.filter(function (Employees) {
      return (
        Employees.name.toLowerCase().includes(Input.toLowerCase()) || Employees.tel.includes(Input)
      )
    })
  }
  return {
    headers,
    form,
    dialog,
    dialogDelete,
    loading,
    initilEmployee,
    editedEmployee,
    Employees,
    editedIndex,
    lastId,
    onSubmit,
    closeDelete,
    deleteItemConfirm,
    editItem,
    deleteItem,
    closeDialog,
    save,
    initialize,
    onMounted,
    searchfor
  }
})
