import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const isloading = ref(false)
  const doLoad = () => {
    isloading.value = true
  }
  const finish = () => {
    isloading.value = false
  }
  return { isloading, doLoad, finish }
})
