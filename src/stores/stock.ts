import type { Stock } from '@/types/Stock'
import { defineStore } from 'pinia'
import { computed, ref } from 'vue'
import { useRouter } from 'vue-router'
import { useLoadingStore } from './loading'
import stockService from '@/components/services/stock'

export const useStockStore = defineStore('stock', () => {
  const loadingStore = useLoadingStore()
  const material = ref<Stock[]>([])
  const search = ref<string>('')
  const initialStock: Stock = {
    name: '',
    status: 'Available',
    min: 0,
    balance: 0,
    unit: ''
  }
  const editedStock = ref<Stock>(JSON.parse(JSON.stringify(initialStock)))

  async function getStocks() {
    loadingStore.doLoad()
    const res = await stockService.getStocks()
    material.value = res.data
    loadingStore.finish()
  }

  async function getStock(id: number) {
    loadingStore.doLoad()
    const res = await stockService.getStock(id)
    editedStock.value = res.data
    loadingStore.finish()
  }

  async function saveStock() {
    loadingStore.doLoad()
    const stock = editedStock.value
    if (!stock.id) {
      console.log('Post' + JSON.stringify(stock))
      const res = await stockService.addStock(stock)
    } else {
      console.log('Patch' + JSON.stringify(stock))
      const res = await stockService.updateStock(stock)
    }
    await getStocks()
    loadingStore.finish()
  }

  async function deleteStock() {
    loadingStore.doLoad()
    const stock = editedStock.value
    const res = await stockService.delStock(stock)
    await getStocks()
    loadingStore.finish()
  }

  const searchStock = computed(() => {
    const searchTerm = search.value.toLowerCase()
    return material.value.filter((item) => {
      return (
        item.name.toLowerCase().includes(searchTerm) ||
        item.status.toLowerCase().includes(searchTerm)
      )
    })
  })

  const totalStocks = computed(() => material.value.length)
  const totalAvailableStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Available').length
  })
  const totalLowStocks = computed((CheckBalance) => {
    return searchStock.value.filter((item) => item.status === 'Low').length
  })
  const totalOutStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Out of Stock').length
  })

  const availableStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Available')
  })
  const lowStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Low')
  })
  const outStocks = computed(() => {
    return searchStock.value.filter((item) => item.status === 'Out of Stock')
  })
  function clearForm() {
    editedStock.value = JSON.parse(JSON.stringify(initialStock))
  }

  return {
    getStocks,
    getStock,
    saveStock,
    deleteStock,
    search,
    totalStocks,
    totalAvailableStocks,
    totalLowStocks,
    totalOutStocks,
    availableStocks,
    lowStocks,
    outStocks,
    searchStock,
    editedStock,
    clearForm
  }
})
