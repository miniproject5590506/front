import type { User } from '@/types/User'
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { useRouter } from 'vue-router'

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([
    {
      id: 1,
      email: 'test',
      password: '1111',
      fullName: 'Kritkhanin Anantakul',
      gender: 'male',
      roles: ['admin']
    },
    {
      id: 2,
      email: 'Wittaya@gmail.com',
      password: '1234',
      fullName: 'Wittaya Judnguhlearm',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 3,
      email: 'Pasinee@gmail.com',
      password: '1234',
      fullName: 'Pasinee Chaweenat',
      gender: 'female',
      roles: ['user']
    },
    {
      id: 4,
      email: 'Jedsada@gmail.com',
      password: '1234',
      fullName: 'Jedsada Nathee',
      gender: 'male',
      roles: ['user']
    },
    {
      id: 5,
      email: 'Arthaphan@gmail.com',
      password: '1234',
      fullName: 'Arthaphan Charoenchaisakulsuk',
      gender: 'male',
      roles: ['user']
    }
  ])

  const username = ref('')
  const password = ref('')
  const router = useRouter()
  const dialogFailed = ref(false)
  const isLoggedIn = ref(false)
  const loggedInUser = ref<{ name: string; email: string } | null>(null)

  function login() {
    const foundUser = users.value.find(
      (item) => item.email === username.value && item.password === password.value
    )

    if (foundUser) {
      console.log('User found:', foundUser)
      router.push('/pos-view')
      setLoggedInUser(foundUser.fullName, foundUser.email)
      isLoggedIn.value = true
    } else {
      console.log('Login failed. User not found.')
      dialogFailed.value = true
      return
    }
  }

  function setLoggedInUser(name: string, email: string) {
    loggedInUser.value = { name, email }
  }

  function closeDialog() {
    dialogFailed.value = false
    clear()
  }

  function clear() {
    username.value = ''
    password.value = ''
    setLoggedInUser('', '')
  }

  function logout() {
    clear()
    router.push('/')
    isLoggedIn.value = false
  }

  return {
    users,
    username,
    password,
    loggedInUser,
    dialogFailed,
    login,
    setLoggedInUser,
    closeDialog,
    logout,
    isLoggedIn
  }
})
