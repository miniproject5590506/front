type Status = 'Available' | 'Low' | 'Out of Stock'

type Stock = {
  id?: number
  name: string
  status: Status
  min: number
  balance: number
  unit: string
}
export type { Status, Stock }
